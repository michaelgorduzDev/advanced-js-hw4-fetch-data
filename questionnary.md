Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AJAX techology allows us to fetch data between server and browser in background mode,
without reloading a page.

Pros: 
1. Speed and user convenience
2. Less traffic
3. Less server loading

Cons: 
1. Project difficulty increase
2. Bad SEO optimization (search engines cannot see AJAX content)