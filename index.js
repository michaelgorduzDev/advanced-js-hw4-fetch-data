const moviesList = document.querySelector(".moviesList");

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        console.log(data);

        data.forEach(movie => {
            const movieName = movie.name;
            const episodeName = movie.episodeId;
            const openingCrawl = movie.openingCrawl;

            const movieElement = document.createElement("p");
            movieElement.classList.add("movie-name");
            const opening = document.createElement("span");
            opening.textContent = `(${openingCrawl})`;
            opening.classList.add("descr");

            movieElement.textContent = `Episode ${episodeName} - ${movieName} `;

            movieElement.appendChild(opening);
            moviesList.appendChild(movieElement);

            // Fetch character data and display their names
            const characters = movie.characters;
            const charactersList = document.createElement("ul");
            charactersList.classList.add("starring");

            Promise.all(characters.map(characterURL => fetch(characterURL).then(response => response.json())))
                .then(charactersData => {
                    charactersData.forEach(characterData => {

                        const characterItem = document.createElement("li");
                        characterItem.textContent = characterData.name;
                        charactersList.appendChild(characterItem);
                    });
                    movieElement.appendChild(charactersList);
                    const separator = document.createElement("hr");
                    movieElement.appendChild(separator);
                })
                .catch(error => console.log('Error fetching character data:', error));
        });
    })
    .catch(error => console.log('Error:', error));
